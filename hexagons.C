void hexagons()
{
  float cellSize=0.65;
  float sqrt3=1.73205;
  int sensorRadius=11*cellSize;
  int nhit=28;
  float zpositions[40]={13.32 , 14.21 , 16.58 , 17.47 , 19.84 , 20.73 , 23.10 , 23.99 , 26.36 , 27.25 , 29.62 , 30.51 , 32.88 , 33.77 , 36.14 , 37.03 , 39.40 , 40.29 , 42.66 , 43.55 , 46.05 , 47.18 , 49.98 , 51.27 , 53.94 , 54.95 , 57.62 , 58.64 , 68.50 , 75.44 , 82.25 , 89.16 , 96.00 , 102.80 , 106.15 , 113.66 , 121.17 , 129.10 , 136.61 , 144.02};
  
  new TGeoManager("pgon", "poza11");
  TGeoMaterial *matVacuum = new TGeoMaterial("Air", 0,0,0);
  TGeoMaterial *matAl = new TGeoMaterial("Si", 26.98,13,2.7);
  matAl->SetTransparency(50);
  TGeoMedium *Vacuum = new TGeoMedium("Air",1, matVacuum);
  TGeoMedium *Al = new TGeoMedium("Si",2, matAl);
  TGeoVolume *top = gGeoManager->MakeBox("TOP",Vacuum,150,150,150);
  gGeoManager->SetTopVolume(top);   
  gGeoManager->GetVolume("TOP")->SetTransparency(100);

  TGeoVolume *vol = gGeoManager->MakePgon("PGON",Al, 0,360.0,6,2);
  TGeoPgon *pgon = (TGeoPgon*)(vol->GetShape());
  pgon->DefineSection(0,0,sensorRadius*sqrt3/2,sensorRadius*sqrt3/2+sensorRadius*sqrt3/2/20.);
  pgon->DefineSection(1,0.05,sensorRadius*sqrt3/2,sensorRadius*sqrt3/2+sensorRadius*sqrt3/2/20);
  gGeoManager->GetVolume("PGON")->SetLineColor(1); 
  gGeoManager->GetVolume("PGON")->SetTransparency(50); 

  //This is EE
  for( int ilayer=0; ilayer<28; ilayer++ ){
    top->AddNode(vol,ilayer+1,new TGeoCombiTrans(0,0,zpositions[ilayer],0));
  }


  int ucoord[]={0,1,0,-1,-1,0,1};
  int vcoord[]={0,0,-1,-1,0,1,1};
  float u[]={0,sqrt3};
  float v[]={1.5,-sqrt3/2};
  for( int ilayer=0; ilayer<12; ilayer++ ){
    for( int ifl=0; ifl<7; ifl++ ){
      float x=(ucoord[ifl]*u[0]+vcoord[ifl]*v[0])*sensorRadius;
      float y=(ucoord[ifl]*u[1]+vcoord[ifl]*v[1])*sensorRadius;
      top->AddNode(vol,28+ifl+ilayer*7,new TGeoCombiTrans(x,y,zpositions[28+ilayer],0));
    }
  }  
  gGeoManager->CloseGeometry();

  TEveManager::Create();

  TGeoNode* node = gGeoManager->GetTopNode();
  TEveGeoTopNode* en = new TEveGeoTopNode(gGeoManager, node);
  en->SetVisLevel(1);
  en->GetNode()->GetVolume()->SetVisibility(kFALSE);
  gEve->AddGlobalElement(en);
  
  gEve->GetViewers()->SwitchColorSet();
  gEve->GetBrowser()->GetTabRight()->SetTab(1);

  TEveQuadSet* q = new TEveQuadSet("HITS");
  q->Reset(TEveQuadSet::kQT_HexagonXY, kFALSE, 32);
  for( int ifl=0; ifl<7; ifl++ ){
    float x=(ucoord[ifl]*u[0]+vcoord[ifl]*v[0])*cellSize/2;
    float y=(ucoord[ifl]*u[1]+vcoord[ifl]*v[1])*cellSize/2;
    q->AddHexagon(x,
		  y,
		  zpositions[0],
		  cellSize/2);
    q->QuadValue(0);
    
  }
  for (Int_t i=1; i<nhit; ++i){
    q->AddHexagon(0,
		  0,
		  zpositions[i],
		  cellSize/2);
    q->QuadValue(0);
  }
  q->RefitPlex();


  
  gEve->AddElement(q);
  gEve->GetDefaultGLViewer()->SetStyle(TGLRnrCtx::kOutline);
  gEve->Redraw3D(kTRUE);

  en->ExpandIntoListTreesRecursively();
  en->Save("hgcal_geom.root", "HGCALGeom");

}
